COMPILER = gcc
SOURCE_FILES = dropboxfs.c

build: $(SOURCE_FILES)
	$(COMPILER) $(SOURCE_FILES) -Wall -o dropboxfs `pkg-config fuse --cflags --libs` -D_FILE_OFFSET_BITS=64
	echo 'To Mount: ./dropboxfs -f [mount point]'

clean:
	fusermount -u ./mount_here
	
test: $(SOURCE_FILES)
	$(COMPILER) $(SOURCE_FILES) -Wall -o dropboxfs `pkg-config fuse --cflags --libs` -D_FILE_OFFSET_BITS=64
	echo 'To Mount: ./dropboxfs -f [mount point]'
	./dropboxfs -f ./mount_here
