/*
  DROPBOXFS: Filesystem in Userspace for Dropbox

  gcc -Wall dropboxfs.c `pkg-config fuse --cflags --libs` -o dropboxfs
*/

#define FUSE_USE_VERSION 26

#include <string.h>
#include <fuse.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <fcntl.h>

#ifndef BUFFER_SIZE
#define BUFFER_SIZE 256
#endif  

#define DBXFS_STATUS_OK 0
#define DBXFS_STATUS_ERROR -1
#define DBXFS_STATUS_PCLOSE_ERROR -2
#define DBXFS_STATUS_CLI_ERROR -3

// string utility functions //

int check_starts_with(const char* a, const char* b)
{
	return strncmp(a, b, strlen(b)) == 0? 1 : 0;
}

char* trim_whitespace(char* in)
{
	size_t size = strlen(in);
	if (!size)
	{
		return in;
	}

	char* end = in + size - 1;
	while (end >= in && isspace(*end))
	{
		end--;
	}
	*(end + 1) = '\0';

	while (*in && isspace(*in))
	{
		in++;
	}
	return in;
}

typedef struct buffer_list
{
	int count;
	char** list;
} buffer_list;

buffer_list* create_buffer_list(int count, char** list)
{
	buffer_list* result = malloc(sizeof(*result));
	if(result == NULL)
	{
		free(result);
		exit(EXIT_FAILURE);
	}
	result->count = count;
	result->list = list;
	return result;
}

void free_buffer_list(buffer_list* buff_list)
{
	for(int i = 0; i < buff_list->count; i++)
	{
		free(buff_list->list[i]);
	}
	free(buff_list->list);
	free(buff_list);
}

void push_buffer_list(buffer_list* buff_list, const char* string)
{
	char** new_list_ptr = 
		realloc(buff_list->list, (buff_list->count + 1) * sizeof(*(buff_list->list)));
	
	if(new_list_ptr == NULL) 
	{
		free_buffer_list(buff_list);
		free(new_list_ptr);
		exit(EXIT_FAILURE);
	}
	buff_list->list = new_list_ptr;
	
	
	char* new_string_ptr = strdup(string);
	if(new_string_ptr == NULL) 
	{
		free_buffer_list(buff_list);
		free(new_string_ptr);
		exit(EXIT_FAILURE);
	}
	buff_list->list[buff_list->count] = new_string_ptr;
	buff_list->count++;
}

void multi_tok(char* in, char* delim, int offset, buffer_list* buff_list)
{
	char* token;

	do
	{
		token = strstr(in,delim);

		if (token != NULL)
		{
			*(token) = '\0';
		}
		
		push_buffer_list(buff_list, trim_whitespace(in));

		in = token + strlen(delim) + offset;
	} 
	while(token != NULL);
}

// ... //


// dbxcli wrapper functions for common utilities  //

char dbxcli_path[BUFFER_SIZE] = "./dbxcli-linux-amd64";
//char dbxcli_path[BUFFER_SIZE] = "dbxcli";
void dbxcli_init()
{
	char user_path[BUFFER_SIZE];

	printf("Path to dbxcli [press enter for default: %s]: ", dbxcli_path);

	if(fgets(user_path, sizeof user_path, stdin) != NULL)
	{
		strtok(user_path, "\n");
		
		if (strcmp(user_path, "\n" ) != 0)
		{
			strcpy(dbxcli_path, user_path);
		}
	}
}

FILE* dbxcli_call(const char* cmd)
{
	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, dbxcli_path);
	strcat(cmd_buff, " 2>&1 "); // redirect stderr to stdout
	strcat(cmd_buff, cmd);

	return popen(cmd_buff, "r");
}

int dbxcli_close(FILE* fp)
{
	int status = DBXFS_STATUS_OK;
	
	int close_status = pclose(fp);
	if (close_status == -1) 
	{
	    /* Error reported by pclose() */
	    status = DBXFS_STATUS_PCLOSE_ERROR;
	} 
	else 
	{
	    /* Use macros described under wait() to inspect `status' in order
	       to determine success/failure of command executed by popen() */
	}

	return status;
}

int dbxcli_account()
{
	int status = DBXFS_STATUS_OK;

	FILE* fp = dbxcli_call("account");
	char account_buff[BUFFER_SIZE];
	
	while(fgets(account_buff, sizeof account_buff, fp) != NULL)
	{
		strtok(account_buff, "\n");
		puts(account_buff);
	}
	
	status = dbxcli_close(fp);
	
	return status;
}

int dbxcli_ls(const char* path, buffer_list* buff_list)
{
	int status = DBXFS_STATUS_OK;
	
	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, "ls");
	strcat(cmd_buff, " '");
	strcat(cmd_buff, path);
	strcat(cmd_buff, "' ");

	FILE* fp = dbxcli_call(cmd_buff);
	
	char ls_buff[BUFFER_SIZE];
	
	while (fgets(ls_buff, sizeof ls_buff, fp) != NULL)
	{	
		strtok(ls_buff, "\n");
		
		if(check_starts_with(ls_buff, "Error"))
		{
			status = DBXFS_STATUS_CLI_ERROR;
            break;
		}
		
		multi_tok(ls_buff, " /", -1, buff_list);
	}
	
    if(status == DBXFS_STATUS_CLI_ERROR)
    {
        dbxcli_close(fp);
        return status;
    }
    else
    {
        status = dbxcli_close(fp);
   	    return status;
    }
}

int dbxcli_mkdir(const char* path)
{
	int status = DBXFS_STATUS_OK;

	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, "mkdir");
	strcat(cmd_buff, " '");
	strcat(cmd_buff, path);
	strcat(cmd_buff, "' ");

	FILE* fp = dbxcli_call(cmd_buff);

	char mkdir_buff[BUFFER_SIZE];

	while (fgets(mkdir_buff, sizeof mkdir_buff, fp) != NULL)
	{	
		strtok(mkdir_buff, "\n");
		
		if(check_starts_with(mkdir_buff, "Error"))
		{
			status = DBXFS_STATUS_CLI_ERROR;
            break;
		}
	}
	
    if(status == DBXFS_STATUS_CLI_ERROR)
    {
        dbxcli_close(fp);
        return status;
    }
    else
    {
        status = dbxcli_close(fp);
   	    return status;
    }
}

int dbxcli_rm(const char* path)
{
	int status = DBXFS_STATUS_OK;

	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, "rm");
	strcat(cmd_buff, " '");
	strcat(cmd_buff, path);
	strcat(cmd_buff, "' ");
	strcat(cmd_buff, "-f"); // force delete non-empty folders

	FILE* fp = dbxcli_call(cmd_buff);

	char rm_buff[BUFFER_SIZE];

	while (fgets(rm_buff, sizeof rm_buff, fp) != NULL)
	{	
		strtok(rm_buff, "\n");
		
		if(check_starts_with(rm_buff, "Error"))
		{
			status = DBXFS_STATUS_CLI_ERROR;
            break;
		}
	}

	if(status == DBXFS_STATUS_CLI_ERROR)
    {
        dbxcli_close(fp);
        return status;
    }
    else
    {
        status = dbxcli_close(fp);
   	    return status;
    }
}

int dbxcli_cp(const char* source, const char* target)
{
	int status = DBXFS_STATUS_OK;

	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, "cp '");
	strcat(cmd_buff, source);
	strcat(cmd_buff, "' '");
	strcat(cmd_buff, target);
	strcat(cmd_buff, "'  ");

	FILE* fp = dbxcli_call(cmd_buff);

	char cp_buff[BUFFER_SIZE];

	while (fgets(cp_buff, sizeof cp_buff, fp) != NULL)
	{	
		strtok(cp_buff, "\n");
		
		if(check_starts_with(cp_buff, "Error"))
		{
			status = DBXFS_STATUS_CLI_ERROR;
            break;
		}
	}

	if(status == DBXFS_STATUS_CLI_ERROR)
    {
        dbxcli_close(fp);
        return status;
    }
    else
    {
        status = dbxcli_close(fp);
   	    return status;
    }
}

int dbxcli_mv(const char* source, const char* target)
{
	int status = DBXFS_STATUS_OK;

	char cmd_buff[BUFFER_SIZE];

	strcpy(cmd_buff, "mv '");
	strcat(cmd_buff, source);
	strcat(cmd_buff, "' '");
	strcat(cmd_buff, target);
	strcat(cmd_buff, "'  ");

	FILE* fp = dbxcli_call(cmd_buff);

	char mv_buff[BUFFER_SIZE];

	while (fgets(mv_buff, sizeof mv_buff, fp) != NULL)
	{	
		strtok(mv_buff, "\n");
		
		if(check_starts_with(mv_buff, "Error"))
		{
			status = DBXFS_STATUS_CLI_ERROR;
            break;
		}
	}

	if(status == DBXFS_STATUS_CLI_ERROR)
    {
        dbxcli_close(fp);
        return status;
    }
    else
    {
        status = dbxcli_close(fp);
   	    return status;
    }
}

// ... //

// FUSE callbacks //

static int dropboxfs_getattr(const char *path, struct stat *stbuf)
{
	int status = DBXFS_STATUS_OK;

	memset(stbuf, 0, sizeof(struct stat));
	if (strcmp(path, "/") == 0) 
	{
			stbuf->st_mode = S_IFDIR | 0755;
			stbuf->st_nlink = 2;
	} 
	else
	{
		buffer_list* content = create_buffer_list(0, NULL);
		status = dbxcli_ls(path, content);
		if (status == DBXFS_STATUS_OK)
		{
			if(content->count==0 || content->count > 1)
			{
				stbuf->st_mode = S_IFDIR | 0755;
				stbuf->st_nlink = 2;
			}
			else if(content->count == 1) 
			{
				if(strcmp(path, content->list[0]) == 0)
				{
					stbuf->st_mode = S_IFREG | 0444;
					stbuf->st_nlink = 1;
					stbuf->st_size = strlen(path);
				}
				else
				{
					stbuf->st_mode = S_IFDIR | 0755;
					stbuf->st_nlink = 2;
				}
			}
		}
		else
		{
			status = -ENOENT;
		}

		free_buffer_list(content);
	}
	
	// printf("[dropboxfs_getattr::{status: %d}]: %s \n",status, path);
	return status;
}

static int dropboxfs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
							 off_t offset, struct fuse_file_info *fi)
{
	(void) offset;
	(void) fi;

	int status = DBXFS_STATUS_OK;

	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);

	buffer_list* content = create_buffer_list(0, NULL);
	status = dbxcli_ls(path, content);
	
	if(status == DBXFS_STATUS_OK) 
	{
		int offset = (strcmp(path,"/") == 0)? 0 : 1; 
		for(int i = 0; i < content->count; i++)
		{
			char* entry = content->list[i] + strlen(path) + offset;
			filler(buf, entry, NULL, 0);
		}
	}
	
	// printf("[dropboxfs_readdir::{status: %d}]: %s \n",status, path);
	free_buffer_list(content);
	return status;
}

static int dropboxfs_mkdir(const char* path, mode_t mode)
{
	int status = DBXFS_STATUS_OK;

	status = dbxcli_mkdir(path);
	if (status != DBXFS_STATUS_OK)
	{
		status = -errno;
	}

	// printf("[dropboxfs_mkdir::{status: %d}]: %s \n",status, path);
	return status;
}

static int dropboxfs_unlink(const char *path)
{
	int status = DBXFS_STATUS_OK;

	status = dbxcli_rm(path);
	if (status != DBXFS_STATUS_OK)
	{
		status = -errno;
	}
	
	// printf("[dropboxfs_unlink::{status: %d}]: %s \n",status, path);
	return status;
}

static int dropboxfs_rmdir(const char* path)
{
	int status = DBXFS_STATUS_OK;

	status = dbxcli_rm(path);
	if (status != DBXFS_STATUS_OK)
	{
		status = -errno;
	}

	//printf("[dropboxfs_rmdir::{status: %d}]: %s \n",status, path);
	return status;
}

// ... //

static struct fuse_operations operations = {
    .getattr	= dropboxfs_getattr,
    .readdir	= dropboxfs_readdir,
    .mkdir		= dropboxfs_mkdir,
	.unlink		= dropboxfs_unlink,
	.rmdir		= dropboxfs_rmdir
	
	//.copy_file_range	~ dbxcli_cp // only available in fuse 3.4.0 
									// which is not built into ubuntu's linux kernel 
									// (requires building libfuse from source)
	
	//					~ dbxcli_mv // no such semantic is build into fuse
};

int main( int argc, char *argv[] )
{
	int status = DBXFS_STATUS_OK;

	dbxcli_init();
	
	dbxcli_account();
	
	status = fuse_main(argc, argv, &operations, NULL);

	return status;
}
